import "react-native-gesture-handler";
import React from "react";
import { StatusBar } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import MainNews from "../Pages/News";
import InShortsNews from "../Pages/InShortNews";
import GujaratNews from "../Pages/Gujaratnews";
const Drawer = createDrawerNavigator();

export default function Application() {
  return (
    <NavigationContainer>
      <StatusBar />
      <Drawer.Navigator drawerLockMode="locked-open" initialRouteName="Home">
        <Drawer.Screen name="News" component={MainNews} />
        <Drawer.Screen name="InShortsNews" component={InShortsNews} />
        <Drawer.Screen name="Gujarat Samachar" component={GujaratNews} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

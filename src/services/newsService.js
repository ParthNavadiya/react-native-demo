import { NEWS_API_KEY, NEWS_BASE_URL } from "../Utils/config";

export const gettopheadlinesNewsList = async () => {
  return await fetch(
    `${NEWS_BASE_URL}/top-headlines?country=in&apiKey=${NEWS_API_KEY}`,
    {
      method: "GET", // *GET, POST, PUT, DELETE, etc.
      headers: {
        "Content-Type": "application/json",
      },
    }
  )
    .then((response) => response.json())
    .then((data) => {
      return data;
    });
};
export const getEverythingNewsList = async () => {
  return await fetch(
    `${NEWS_BASE_URL}/everything?q=bitcoin&apiKey=${NEWS_API_KEY}`,
    {
      method: "GET", // *GET, POST, PUT, DELETE, etc.
      headers: {
        "Content-Type": "application/json",
      },
    }
  )
    .then((response) => response.json())
    .then((data) => {
      return data;
    });
};
export const getSourcesNewsList = async () => {
  return await fetch(
    `https://newsapi.org/v2/sources?apiKey=8067936432ad4c78b037ca9f06ba12ae`,
    {
      method: "GET", // *GET, POST, PUT, DELETE, etc.
      headers: {
        "Content-Type": "application/json",
      },
    }
  )
    .then((response) => response.json())
    .then((data) => {
      return data;
    });
};

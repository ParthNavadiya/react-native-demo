export const getAlltrendingTopicList = async () => {
  return await fetch(
    "https://www.gujaratsamachar.com/api/categories?parentId=0&type=article",
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  )
    .then((response) => response.json())
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.error(err);
    });
};

export const getNewsList = async (item) => {
  return await fetch(
    `https://www.gujaratsamachar.com/api/stories?categoryId=${item._id}`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  )
    .then((response) => response.json())
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.error(err);
    });
};

export const getNewsDetail = async (item) => {
  return await fetch(
    `https://www.gujaratsamachar.com/api/article-details/${item.categorySlug}/${item.articleUrl}`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  )
    .then((response) => response.json())
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.error(err);
    });
};

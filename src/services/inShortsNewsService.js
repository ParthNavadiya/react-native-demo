export const getAlltrendingTopicList = async () => {
  return await fetch("https://inshorts.com/api/en/search/trending_topics", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.error(err);
    });
};

export const getNewsList = async (item) => {
  return await fetch(
    `https://inshorts.com/api/en/search/trending_topics/${item.tag}?type=NEWS_CATEGORY`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  )
    .then((response) => response.json())
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.error(err);
    });
};

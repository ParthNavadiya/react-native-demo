import "react-native-gesture-handler";
import React, { useEffect, useState } from "react";
import { View, ActivityIndicator } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import {
  getAlltrendingTopicList,
  getNewsList,
} from "../../services/inShortsNewsService";
import InshortNewsComponent from "../../components/InshortNewsComponent";
import InSortNewsDetails from "./DetailScreen";
import NavigationDrawerStructure from "../../components/CommonComponent/NavigationDrawer";
import NavigationBack from "../../components/CommonComponent/NavigationBackComponent";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

function TopnewsScreen({ navigation, route }) {
  const [newsList, setNewsList] = useState([]);
  const [loader, setLoader] = useState(false);
  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", async () => {
      setNewsList([]);
      setLoader(true);
      const response = await getNewsList(route.params.item);
      if (response) {
        setNewsList(response.data.news_list);
        setLoader(false);
      }
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    if (route.params.item.priority == 0) {
      const fetchData = async () => {
        setLoader(true);
        const response = await getNewsList(route.params.item);
        if (response) {
          setNewsList(response.data.news_list);
          setLoader(false);
        }
      };
      fetchData();
    }
  }, []);

  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      {loader && <ActivityIndicator size="large" color="#0000ff" />}
      <InshortNewsComponent newsList={newsList} navigation={navigation} />
    </View>
  );
}

const Stack = createStackNavigator();
const Tab = createMaterialTopTabNavigator();

function Tabs({ navigation }) {
  const [topicList, setTopicList] = useState([]);
  const [loader, setLoader] = useState(false);
  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", async () => {
      setLoader(true);
      const response = await getAlltrendingTopicList();
      if (response) {
        setTopicList(response.data.trending_tags);
        setLoader(false);
      }
    });

    return unsubscribe;
  }, [navigation]);

  const renderScreen = () => {
    let result = null;
    if (topicList) {
      result = topicList.map((item, index) => {
        return (
          <Tab.Screen
            key={index}
            name={item.tag}
            component={TopnewsScreen}
            initialParams={{ item: item }}
            options={{ tabBarLabel: item.label }}
          />
        );
      });
      return result;
    }
  };

  return (
    <>
      {loader && <ActivityIndicator size="large" color="#0000ff" />}
      {topicList.length !== 0 && (
        <Tab.Navigator
          initialRouteName={topicList[0].tag}
          tabBarOptions={{
            activeTintColor: "#e91e63",
            inactiveTintColor: "#fff",
            scrollEnabled: true,
            labelStyle: { fontSize: 14, fontWeight: "bold" },
            style: { backgroundColor: "#633689" },
          }}
        >
          {renderScreen()}
        </Tab.Navigator>
      )}
    </>
  );
}

export default function InShortsNews({ navigation }) {
  return (
    <Stack.Navigator
      initialRouteName="Tabs"
      screenOptions={{
        headerLeft: () => (
          <NavigationDrawerStructure navigationProps={navigation} />
        ),
        headerStyle: { backgroundColor: "#633689" },
        headerTintColor: "#fff",
        headerTitleStyle: { fontWeight: "bold", textAlign: "center" },
      }}
    >
      <Stack.Screen
        name="Tabs"
        component={Tabs}
        options={{ title: "My News" }}
      />
      <Stack.Screen
        name="InSortNewsDetails"
        component={InSortNewsDetails}
        options={({ navigation }) => ({
          title: "My News Detail",
          headerLeft: () => (
            <NavigationBack navigationProps={navigation} />
          ),
        })}
      />
    </Stack.Navigator>
  );
}

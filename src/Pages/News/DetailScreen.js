import React from "react";

import {
  StyleSheet,
  View,
  Dimensions,
  ScrollView,
  Image,
  Text,
} from "react-native";

let deviceWidth = Dimensions.get("window").width;

export default function Details({ route, navigation }) {
  const { iteminfo } = route.params;
  return (
    <View style={[styles.container]}>
      <ScrollView>
        <View style={{ alignItems: "center", marginHorizontal: 30 }}>
          <Image
            style={styles.productImg}
            source={{
              uri: iteminfo.item.urlToImage,
            }}
          />
          <Text style={styles.name}>{iteminfo.item.title}</Text>
          <Text style={styles.price}>{iteminfo.item.author}</Text>
          <Text style={styles.description}>{iteminfo.item.description}</Text>
        </View>
      </ScrollView>
    </View>
  );
}

let styles = StyleSheet.create({
  container: {
    width: deviceWidth,
    flex: 1,
    marginTop: 20,
    backgroundColor: "#f4f4f4",
  },
  productImg: {
    width: "100%",
    height: 250,
  },
  name: {
    marginTop: 20,
    fontSize: 20,
    color: "#696969",
    fontWeight: "bold",
  },
  price: {
    marginTop: 10,
    fontSize: 8,
    color: "green",
    fontWeight: "bold",
  },
  description: {
    textAlign: "center",
    marginTop: 10,
    color: "#696969",
  },
});

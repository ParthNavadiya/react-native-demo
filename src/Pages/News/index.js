import "react-native-gesture-handler";
import React, { useEffect, useState } from "react";
import { View, ActivityIndicator } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import {
  gettopheadlinesNewsList,
  getEverythingNewsList,
  getSourcesNewsList,
} from "../../services/newsService";
import Main from "../../components/Main";
import SourceComponent from "../../components/SourceComponent";
import Details from "./DetailScreen";
import NavigationDrawerStructure from "../../components/CommonComponent/NavigationDrawer";
import NavigationBack from "../../components/CommonComponent/NavigationBackComponent";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

function topHeadlineScreen({ navigation }) {
  const [newsList, setNewsList] = useState([]);
  const [loader, setLoader] = useState(false);
  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", async () => {
      setLoader(true);
      const response = await gettopheadlinesNewsList();
      if (response) {
        setNewsList(response.articles);
        setLoader(false);
      }
    });
    return unsubscribe;
  }, [navigation]);

  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      {loader && <ActivityIndicator size="large" color="#0000ff" />}
      <Main newsList={newsList} navigation={navigation} />
    </View>
  );
}

const everrythingScreen = ({ navigation }) => {
  const [newsList, setNewsList] = useState([]);
  const [loader, setLoader] = useState(false);
  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", async () => {
      setLoader(true);
      const response = await getEverythingNewsList();
      if (response) {
        setLoader(false);
        setNewsList(response.articles);
      }
    });
    return unsubscribe;
  }, [navigation]);

  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      {loader && <ActivityIndicator size="large" color="#0000ff" />}
      <Main newsList={newsList} navigation={navigation} />
    </View>
  );
};

function sourceScreen({ navigation }) {
  const [newsList, setNewsList] = useState([]);
  const [loader, setLoader] = useState(false);
  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", async () => {
      setLoader(true);
      const response = await getSourcesNewsList();
      if (response) {
        setLoader(false);
        setNewsList(response.sources);
      }
    });

    return unsubscribe;
  }, [navigation]);

  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      {loader && <ActivityIndicator size="large" color="#0000ff" />}
      <SourceComponent newsList={newsList} navigation={navigation} />
    </View>
  );
}

const Stack = createStackNavigator();
const Tab = createMaterialTopTabNavigator();

function Tabs() {
  return (
    <Tab.Navigator
      initialRouteName="top"
      tabBarOptions={{
        activeTintColor: "#e91e63",
        inactiveTintColor: "#fff",
        labelStyle: { fontSize: 14, fontWeight: "bold" },
        style: { backgroundColor: "#633689" },
      }}
    >
      <Tab.Screen
        name="top"
        component={topHeadlineScreen}
        options={{ tabBarLabel: "Top Headlines" }}
      />
      <Tab.Screen
        name="every"
        component={everrythingScreen}
        options={{ tabBarLabel: "Everything" }}
      />
      <Tab.Screen
        name="source"
        component={sourceScreen}
        options={{ tabBarLabel: "Source" }}
      />
    </Tab.Navigator>
  );
}

export default function MainNews({ navigation }) {
  return (
    <Stack.Navigator
      initialRouteName="Tabs"
      screenOptions={{
        headerLeft: () => (
          <NavigationDrawerStructure navigationProps={navigation} />
        ),
        headerStyle: { backgroundColor: "#633689" },
        headerTintColor: "#fff",
        headerTitleStyle: { fontWeight: "bold", textAlign: "center" },
      }}
    >
      <Stack.Screen
        name="Tabs"
        component={Tabs}
        options={{ title: "My News" }}
      />
      <Stack.Screen
        name="Details"
        component={Details}
        options={({ navigation }) => ({
          title: "My News Detail",
          headerLeft: () => (
            <NavigationBack navigationProps={navigation} />
          ),
        })}
      />
    </Stack.Navigator>
  );
}

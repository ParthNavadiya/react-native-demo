import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  ScrollView,
  Image,
  Text,
  ActivityIndicator,
} from "react-native";
import HTML from "react-native-render-html";
import HTMLView from "react-native-htmlview";
import { getNewsDetail } from "../../services/gujaratNewsService";

let deviceWidth = Dimensions.get("window").width;

export default function InSortNewsDetails({ route, navigation }) {
  const [newsDetail, setNewsDetail] = useState(null);
  const [loader, setLoader] = useState(false);
  useEffect(() => {
    async function fetchData() {
      setNewsDetail(null);
      setLoader(true);
      const response = await getNewsDetail(route.params.iteminfo.item);
      if (response) {
        setNewsDetail(response.data.article[0]);
        setLoader(false);
      }
    }
    // Return the function to unsubscribe from the event so it gets removed on unmount
    fetchData();
  }, []);

  const renderNode = (node, index, siblings, parent, defaultRenderer) => {
    if (node.name === "img") {
      const a = node.attribs;
      const imageHtml = `<img src="${a.src}" />`;
      return (
        <HTML
          source={{
            html: imageHtml,
          }}
        />
      );
    }
  };

  return (
    <View style={[styles.container]}>
      {loader && <ActivityIndicator size="large" color="#0000ff" />}
      {newsDetail && (
        <ScrollView>
          <View style={{ alignItems: "center", marginHorizontal: 30 }}>
            <Text style={styles.name}>{newsDetail.heading}</Text>
            <Text style={styles.price}>{newsDetail.subHeadingOne}</Text>
            <HTMLView value={newsDetail.content} renderNode={renderNode} />
          </View>
        </ScrollView>
      )}
    </View>
  );
}

let styles = StyleSheet.create({
  container: {
    width: deviceWidth,
    flex: 1,
    marginTop: 20,
    backgroundColor: "#f4f4f4",
  },
  productImg: {
    width: "100%",
    height: 250,
  },
  name: {
    marginTop: 20,
    fontSize: 20,
    color: "#696969",
    fontWeight: "bold",
  },
  price: {
    marginTop: 10,
    fontSize: 8,
    color: "green",
    fontWeight: "bold",
  },
  description: {
    textAlign: "center",
    marginTop: 10,
    color: "#696969",
  },
});

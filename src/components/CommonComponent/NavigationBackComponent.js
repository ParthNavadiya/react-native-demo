import "react-native-gesture-handler";
import React from "react";
import { View, TouchableOpacity, Image } from "react-native";

const NavigationBack = (props) => {
  return (
    <View style={{ flexDirection: "row" }}>
      <TouchableOpacity onPress={() => props.navigationProps.goBack()}>
        <Image
          source={require('../../../assets/back.png') }
          style={{
            width: 20,
            height: 20,
            marginLeft: 10,
          }}
        />
      </TouchableOpacity>
    </View>
  );
};

export default NavigationBack;

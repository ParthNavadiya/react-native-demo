import React from "react";
import Carousel from "react-native-snap-carousel";
import {
  StyleSheet,
  View,
  Dimensions,
  FlatList,
  TouchableOpacity,
  Image,
  Text,
  Icon,
} from "react-native";

let deviceWidth = Dimensions.get("window").width;

export default function InshortNewsComponent({ newsList, navigation  }) {
  const getListViewItem = (item) => {
    navigation.navigate("InSortNewsDetails", {
      iteminfo: item,
    });
  };
  return (
    <View style={[styles.container]}>
      <FlatList
        data={newsList}
        renderItem={(news, sectionId, rowId) => {
          return (
            <TouchableOpacity onPress={() => getListViewItem(news)}>
              <View style={styles.card}>
                <View>
                  <Image
                    key={rowId}
                    style={styles.avatar}
                    source={{
                      uri: news.item.news_obj?.image_url,
                    }}
                  />
                </View>
                <View style={styles.description}>
                  <View style={styles.firstRow}>
                    <Text style={styles.username}>
                      {news.item.news_obj?.bottom_headline}
                    </Text>
                    <Text style={styles.username}>{news.item.publishedAt}</Text>
                  </View>
                  <Text style={styles.title}>{news.item.news_obj?.title}</Text>
                </View>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
}

let styles = StyleSheet.create({
  container: {
    width: deviceWidth,
    backgroundColor: "#f4f4f4",
  },
  progressbar: {
    marginTop: 10,
    alignItems: "center",
  },
  card: {
    flexDirection: "row",
    padding: 10,
    marginLeft: 10,
    marginRight: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#CCCCCC",
  },
  avatar: {
    padding: 10,
    width: 50,
    height: 50,
  },
  description: {
    flex: 1,
    marginLeft: 10,
    flexDirection: "column",
  },
  firstRow: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  username: {
    fontSize: 10,
  },
  title: {
    flex: 1,
    flexWrap: "wrap",
    color: "#000",
    fontSize: 12,
  },
  countContainer: {
    flexDirection: "row",
  },
  count: {
    fontSize: 10,
  },
});

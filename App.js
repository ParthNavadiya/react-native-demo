import React from "react";
import Application from "./src/containers/Application";

export default function App() {
  return <Application />;
}
